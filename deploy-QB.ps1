
[CmdletBinding()]
Param (
    [Parameter(Mandatory=$false)]
    [ValidateSet('Install','Uninstall','Repair')]
    [string]$DeploymentType = 'Install',
    [Parameter(Mandatory=$false)]
    [ValidateSet('Interactive','Silent','NonInteractive')]
    [string]$DeployMode = 'Interactive',
    [Parameter(Mandatory=$false)]
    [switch]$AllowRebootPassThru = $false,
    [Parameter(Mandatory=$false)]
    [switch]$TerminalServerMode = $false,
    [Parameter(Mandatory=$false)]
    [switch]$DisableLogging = $false
)

Try {
    ## Set the script execution policy for this process
    Try { Set-ExecutionPolicy -ExecutionPolicy 'ByPass' -Scope 'Process' -Force -ErrorAction 'Stop' } Catch {}

    ##*===============================================
    ##* VARIABLE DECLARATION
    ##*===============================================
    ## Variables: Application
    [string]$appVendor = 'Intuit'
    [string]$appName = 'QuickBooks Enterprise'
    [string]$appVersion = '2023'
    [string]$appArch = ''
    [string]$appLang = ''
    [string]$appRevision = ''
    [string]$appScriptVersion = '1.0.0'
    [string]$appScriptDate = 'XX/XX/20XX'
    [string]$appScriptAuthor = 'Instalaltion'
    ##*===============================================
    ## Variables: Install Titles (Only set here to override defaults set by the toolkit)
    [string]$installName = ''
    [string]$installTitle = 'Intuit QuickBooks Enterprise 2023'

    ##* Do not modify section below
    #region DoNotModify

    ## Variables: Exit Code
    [int32]$mainExitCode = 0

    ## Variables: Script
    [string]$deployAppScriptFriendlyName = 'Deploy Application'
    [version]$deployAppScriptVersion = [version]'3.8.4'
    [string]$deployAppScriptDate = '26/01/2021'
    [hashtable]$deployAppScriptParameters = $psBoundParameters

    ## Variables: Environment
    If (Test-Path -LiteralPath 'variable:HostInvocation') { $InvocationInfo = $HostInvocation } Else { $InvocationInfo = $MyInvocation }
    [string]$scriptDirectory = Split-Path -Path $InvocationInfo.MyCommand.Definition -Parent

    ## Dot source the required App Deploy Toolkit Functions
    Try {
        [string]$moduleAppDeployToolkitMain = "$scriptDirectory\AppDeployToolkit\AppDeployToolkitMain.ps1"
        If (-not (Test-Path -LiteralPath $moduleAppDeployToolkitMain -PathType 'Leaf')) { Throw "Module does not exist at the specified location [$moduleAppDeployToolkitMain]." }
        If ($DisableLogging) { . $moduleAppDeployToolkitMain -DisableLogging } Else { . $moduleAppDeployToolkitMain }
    }
    Catch {
        If ($mainExitCode -eq 0){ [int32]$mainExitCode = 60008 }
        Write-Error -Message "Module [$moduleAppDeployToolkitMain] failed to load: `n$($_.Exception.Message)`n `n$($_.InvocationInfo.PositionMessage)" -ErrorAction 'Continue'
        ## Exit the script, returning the exit code to SCCM
        If (Test-Path -LiteralPath 'variable:HostInvocation') { $script:ExitCode = $mainExitCode; Exit } Else { Exit $mainExitCode }
    }

    #endregion
    ##* Do not modify section above
    ##*===============================================
    ##* END VARIABLE DECLARATION
    ##*===============================================

    If ($deploymentType -ine 'Uninstall' -and $deploymentType -ine 'Repair') {
        ##*===============================================
        ##* PRE-INSTALLATION
        ##*===============================================
        [string]$installPhase = 'Pre-Installation'

        ## Show Welcome Message, Close QuickBooks With a 60 Second Countdown Before Automatically Closing
        Show-InstallationWelcome -CloseApps 'IntuitDataProtect,QBW,QBW32,qbupdate,QBIDPService,Intuit.QBDT.Webconnector.Application,Intuit.QBDT.Webconnector.QBWCMonitor,Intuit.QuickBooks.FCS,QBCFMonitorService' -CloseAppsCountdown 60

        ## Show Progress Message (With a Message to Indicate the Application is Being Uninstalled)
        Show-InstallationProgress -StatusMessage "Removing Any Existing Version of QuickBooks Enterprise 2023. Please Wait..."

        ## Remove Any Existing Version of QuickBooks Enterprise 2023
        $AppList = Get-InstalledApplication -Name 'QuickBooks Enterprise*23*' -WildCard    
        ForEach ($App in $AppList)
        {
        If($App.UninstallString)
        {
        $GUID = $App.UninstallString -replace 'MsiExec.exe /I ', '' -replace 'MsiExec.exe /X ', ''
        Write-log -Message "Found $($App.DisplayName) $($App.DisplayVersion) and a valid uninstall string, now attempting to uninstall."       
        Execute-Process -Path "$exeMsiexec" -Parameters "/x $GUID REBOOT=ReallySuppress /qb! /L*v C:\Windows\Logs\Software\QuickBooksEnterprise2023-Uninstall.log" -WindowStyle Hidden
        Start-Sleep -Seconds 5
        }
        }

        ## Remove QuickBooks Runtime Redistributable
        Remove-MSIApplications "QuickBooks Runtime Redistributable"
   
        ##*===============================================
        ##* INSTALLATION
        ##*===============================================
        [string]$installPhase = 'Installation'

        ## Install QuickBooks Enterprise 2023
        $ExePath = Get-ChildItem -Path "$dirFiles" -Include QuickBooksEnterprise23.exe -File -Recurse -ErrorAction SilentlyContinue
        If($ExePath.Exists)
        {
        Write-Log -Message "Found $($ExePath.FullName), now attempting to install $installTitle."
        Show-InstallationProgress "Installing QuickBooks Enterprise 2023. This may take some time. Please wait..."
        Execute-Process -Path "$ExePath" -Parameters "-s -a QBMIGRATOR=1 MSICOMMAND=/s QB_PRODUCTNUM=XXX-XXX QB_LICENSENUM=XXXX-XXXX-XXXX-XXX" -WindowStyle Hidden
        Start-Sleep -Seconds 5

        ## Workaround for "An internal file in QuickBooks has become unreadable [PS107]" Error Message
        $ESGServices = Get-ChildItem -Path "$dirFiles" -Include ESGServices.dat -File -Recurse -ErrorAction SilentlyContinue
        If($ESGServices.Exists)
        {
        Write-Log -Message "Applying workaround for ""An internal file in QuickBooks has become unreadable [PS107]"" error message."
        Copy-File -Path "$ESGServices" -Destination "$envProgramFiles\Intuit\QuickBooks Enterprise Solutions 23.0\Components\Payroll\ESGServices.dat"
        }

        ## Disable QuickBooks Automatic Updates
        $QBchan = Get-ChildItem -Path "$dirFiles" -Include QBchan.dat -File -Recurse -ErrorAction SilentlyContinue
        If($QBchan.Exists)
        {
        Write-Log -Message "Disabling QuickBooks Automatic Updates."
        Copy-File -Path "$QBchan" -Destination "$envAllUsersProfile\Intuit\QuickBooks Enterprise Solutions 23.0\Components\QBUpdate\QBchan.dat"
        }

        ## Suppress "How QuickBooks Desktop Uses Your Internet Connection" Window
        [scriptblock]$HKCURegistrySettings = {
        Write-Log -Message "Suppressing the ""How QuickBooks Desktop Uses Your Internet Connection"" Window."
        Set-RegistryKey -Key 'HKCU\Software\Intuit\QuickBooks\33.0\bel' -Name 'Dlss' -Value 'RC1MP9/3AX' -Type String -SID $UserProfile.SID
        Set-RegistryKey -Key 'HKCU\Software\Intuit\QuickBooks\33.0\belacct' -Name 'Dlss' -Value 'RC1MP9/3AX' -Type String -SID $UserProfile.SID
        }
        Invoke-HKCURegistrySettingsForAllUsers -RegistrySettings $HKCURegistrySettings -ErrorAction SilentlyContinue
        }
       
        ##*===============================================
        ##* POST-INSTALLATION
        ##*===============================================
        [string]$installPhase = 'Post-Installation'

    }
    ElseIf ($deploymentType -ieq 'Uninstall')
    {
        ##*===============================================
        ##* PRE-UNINSTALLATION
        ##*===============================================
        [string]$installPhase = 'Pre-Uninstallation'

        ## Show Welcome Message, Close QuickBooks With a 60 Second Countdown Before Automatically Closing
        Show-InstallationWelcome -CloseApps 'IntuitDataProtect,QBW,QBW32,qbupdate,QBIDPService,Intuit.QBDT.Webconnector.Application,Intuit.QBDT.Webconnector.QBWCMonitor,Intuit.QuickBooks.FCS,QBCFMonitorService' -CloseAppsCountdown 60

        ## Show Progress Message (With a Message to Indicate the Application is Being Uninstalled)
        Show-InstallationProgress -StatusMessage "Uninstalling the $installTitle Application. Please Wait..."

        ##*===============================================
        ##* UNINSTALLATION
        ##*===============================================
        [string]$installPhase = 'Uninstallation'

        ## Uninstall Any Existing Version of QuickBooks Enterprise 2023
        $AppList = Get-InstalledApplication -Name 'QuickBooks Enterprise*23*' -WildCard    
        ForEach ($App in $AppList)
        {
        If($App.UninstallString)
        {
        $GUID = $App.UninstallString -replace 'MsiExec.exe /I ', '' -replace 'MsiExec.exe /X ', ''
        Write-log -Message "Found $($App.DisplayName) $($App.DisplayVersion) and a valid uninstall string, now attempting to uninstall."       
        Execute-Process -Path "$exeMsiexec" -Parameters "/x $GUID REBOOT=ReallySuppress /qb! /L*v C:\Windows\Logs\Software\QuickBooksEnterprise2023-Uninstall.log" -WindowStyle Hidden
        Start-Sleep -Seconds 5
        }
        }

        ## Uninstall QuickBooks Runtime Redistributable
        Remove-MSIApplications "QuickBooks Runtime Redistributable"

        ##*===============================================
        ##* POST-UNINSTALLATION
        ##*===============================================
        [string]$installPhase = 'Post-Uninstallation'


    }
    ElseIf ($deploymentType -ieq 'Repair')
    {
        ##*===============================================
        ##* PRE-REPAIR
        ##*===============================================
        [string]$installPhase = 'Pre-Repair'


        ##*===============================================
        ##* REPAIR
        ##*===============================================
        [string]$installPhase = 'Repair'


        ##*===============================================
        ##* POST-REPAIR
        ##*===============================================
        [string]$installPhase = 'Post-Repair'


    }
    ##*===============================================
    ##* END SCRIPT BODY
    ##*===============================================

    ## Call the Exit-Script function to perform final cleanup operations
    Exit-Script -ExitCode $mainExitCode
}
Catch {
    [int32]$mainExitCode = 60001
    [string]$mainErrorMessage = "$(Resolve-Error)"
    Write-Log -Message $mainErrorMessage -Severity 3 -Source $deployAppScriptFriendlyName
    Show-DialogBox -Text $mainErrorMessage -Icon 'Stop'
    Exit-Script -ExitCode $mainExitCode
}